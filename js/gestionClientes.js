var clientesObtenidos;

function getClientes() {
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function()
  {
    if (this.readyState ==4 && this.status == 200)
    {
      clientesObtenidos =request.responseText;
      procesarClientes ();

    }
  }
  request.open("GET",url,true);
  request.send();
  }
  function procesarClientes() {
    var rutaBandera;


    var JSONClientes = JSON.parse(clientesObtenidos);
  //  alert(JSONProductos.value[0].ProductName);
  var divTabla=document.getElementById("divTablaCliente");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");
  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i = 0; i < JSONClientes.value.length; i++)
  {
    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");
    columnaNombre.innerText=JSONClientes.value[i].ContactName;

    var columnaCity = document.createElement("td");
    columnaCity.innerText=JSONClientes.value[i].City;

    var columnaPhone = document.createElement("td");
    columnaPhone.innerText=JSONClientes.value[i].Phone;

    var columnaCountry = document.createElement("td");
    var bandera = columnaCountry.innerText=JSONClientes.value[i].Country;
    rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-"

    var columnaBandera = document.createElement("td");
    var imgBandera =document.createElement("img");
    imgBandera.classList.add("flag");
    if(bandera=="UK")
    {
      imgBandera.src = rutaBandera+"United-Kingdom.png"
    }
    else
    {
      imgBandera.src = rutaBandera+bandera+".png"
    }



    nuevaFila.append(columnaNombre);
    nuevaFila.append(columnaCity);
    nuevaFila.append(columnaPhone);
    columnaBandera.appendChild(imgBandera);
    nuevaFila.append(columnaBandera);

    tbody.appendChild(nuevaFila)
  }
    tabla.appendChild(tbody);
    divTabla.append(tabla);

}




// Entrega  SDLC-Proyecto
